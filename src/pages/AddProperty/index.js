import React, { useState } from 'react';
import Head from '../../components/Head';
import { useHistory } from 'react-router-dom';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import { Formik } from 'formik';
import * as yup from 'yup';
import { VALID_IMAGE_FILE_TYPES } from "../../utils/constant";
import toastMessage from '../../utils/toastMessage';

const schema = yup.object().shape({
	propertyName: yup.string().min(2, 'Too short, add atlest 2 chars!').max(50, 'Too long, add atmost 50 chars!').required('Please provide property name!'),
	description: yup.string().min(10, 'Too short, add atlest 10 chars!').max(500, 'Too long, add atmost 500 chars!').required('Please provide description!'),
	address: yup.string().min(5, 'Too short, add atlest 5 chars!').max(200, 'Too long, add atmost 200 chars!').required('Please provide address!'),
	area: yup.string().min(2, 'Too short, add atlest 2 chars!').max(50, 'Too long, add atmost 50 chars!').required('Please provide area!'),
	price: yup.string().required('Please provide price!'),
	bedroom: yup.string().required('Please provide bedroom!'),
	bathroom: yup.string().required('Please provide bathroom!'),
	carpetArea: yup.string().required('Please provide carpet area!')
});

const AddProperty = (props) => {
	const { addProperty } = props;
	const history = useHistory();
	const [images, setImages] = useState([]);
	const [showMaxImageError, setShowMaxImageError] = useState(false);
	const [imageError, setImageError] = useState('');

	const handleBackToList = () => {
		history.push('/');
	}

	const handleImageupload = (event) => {
		setShowMaxImageError(false);
		const oldValueIndex = images.length;
		const values = [...images];
		const newImages = Array.from(event.target.files);
		const totalImagesLength = oldValueIndex + newImages.length;
		if (totalImagesLength > 5) {
			setShowMaxImageError(true);
			setImageError("Maximum only 5 Images are allow to added");
		}
		else if (newImages?.length > 0) {
			newImages.forEach(async (element, index) => {
				const valueIndex = oldValueIndex + index;
				const data = {
					index: index,
					isError: false,
					message: '',
					file: element,
				};
				values.push(data);
				const { type, size } = element;
				if (!VALID_IMAGE_FILE_TYPES.includes(type)) {
					values[valueIndex].isError = true;
					values[valueIndex].message = 'Only upload png, jpg and jpeg files!';
				}
				const fileSize = size / 1024 / 1024;
				if (fileSize > 10) {
					values[valueIndex].isError = true;
					values[valueIndex].message = 'Only upload Images less than 10 MB!';
				}
				setImages([...values]);
			});
		}
	}

	const uploadImage = (event) => {
		event.preventDefault();
		document.getElementById('selectImage').click();
	}

	const deleteImage = (indexValue) => {
		const filterImages = images.filter((item, index) => index !== indexValue);
		setImages(filterImages);
	}

	const handlePropertyFormSubmit = async (values, actions) => {
		if (values) {
			let finalData = values;
			finalData['images'] = images.map((file) => {
				return file.file;
			});
			const formData = new FormData();
			for (const file of finalData.images) {
				formData.append('images', file);
			}
			Object.keys(finalData).forEach((key) => {
				if(key !== "images"){
					formData.append(key, finalData[key])
				}
			})
			const saveProperty = await addProperty(formData);
			toastMessage('success', 'Property has been added successfuly');
			history.push('/');
		}
	}

	return (
		<>
			<Head title="Add Property" />
			<div className="p-3" style={{ height: "100vh", backgroundColor: "#F5F5F5" }}>
				<div className="card mb-3" style={{ width: "100%" }}>
					<div className="card-body d-flex">
						<div className="flex-grow-1 align-self-center">
							<h5 className="card-title">Add Property with Real Estate!</h5>
							<p className="card-text">Here you add property by providing details like property name, images, location and others.</p>
						</div>
						<button className="btn btn-primary align-self-center" onClick={() => handleBackToList()}>Back to List</button>
					</div>
				</div>
				<div className="row">
					<div className="col">
						<div className="card mb-5">
							<div className="card-body">
								<h5 className="card-title text-center pb-3" style={{ borderBottom: "1px #212529 solid" }}>Add Property</h5>
								<div className="mt-6 p-5">
									<Formik
										validationSchema={schema}
										onSubmit={(values, actions) => { handlePropertyFormSubmit(values, actions) }}
										initialValues={{
											propertyName: '',
											description: '',
											address: '',
											area: '',
											price: '',
											bedroom: '3',
											bathroom: '3',
											carpetArea: 'sq_ft'
										}}
									>
										{({
											handleSubmit,
											handleChange,
											handleBlur,
											values,
											touched,
											isValid,
											errors,
										}) => (
											<Form noValidate onSubmit={handleSubmit} enctype="multipart/form-data">
												<Form.Row>
													<Form.Group as={Col} md="4">
														<div className="mb-2">
															<Form.Label>Property Images</Form.Label>
															{showMaxImageError && <div className="error-msg mb-2">
																{imageError}
															</div>}
															<div className="d-flex flex-wrap mt-2">
																{images?.map((fileData, index) => (
																	<div key={index} className="mr-4 mb-4 object-cover position-relative">
																		{fileData.isError ? (
																			<div
																				className="d-flex align-items-center text-center file-upload-add-button error-msg"
																			>
																				<span>{fileData.message}</span>
																			</div>
																		) : (
																			<img
																				className="object-cover"
																				src={URL.createObjectURL(fileData.file)}
																			/>
																		)}
																		<Button
																			className="btn btn-danger position-absolute image-delete-button"
																			type="button"
																			onClick={() => deleteImage(index)}
																		>
																			<i className="fa fa-close"></i>
																		</Button>
																	</div>
																))}
																<input type="file" id="selectImage" hidden onChange={(event) => handleImageupload(event)} multiple />
																{images && images.length < 5 && <div
																	className="d-flex align-items-center file-upload-add-button"
																	onClick={(event) => uploadImage(event)}
																>
																	<i className="fa fa-plus"></i>
																</div>}
															</div>
														</div>
													</Form.Group>
													<Form.Group as={Col} md="4" controlId="validationFormik02">
														<div className="mb-2">
															<Form.Label>Property Name</Form.Label>
															<Form.Control
																type="text"
																name="propertyName"
																placeholder="Property Name"
																value={values.propertyName}
																onChange={handleChange}
																isValid={touched.propertyName && !errors.propertyName}
															/>
															{errors.propertyName && touched.propertyName &&
																<div className="error-msg">
																	{errors.propertyName}
																</div>
															}
														</div>
														<div className="mb-2">
															<Form.Label>Description</Form.Label>
															<Form.Control
																as="textarea"
																rows={3}
																name="description"
																placeholder="Description"
																value={values.description}
																onChange={handleChange}
																isValid={touched.description && !errors.description}
															/>
															{errors.description && touched.description &&
																<div className="error-msg">
																	{errors.description}
																</div>
															}
														</div>
														<div className="mb-2">
															<Form.Label>Address</Form.Label>
															<Form.Control
																as="textarea"
																rows={3}
																name="address"
																placeholder="Address"
																value={values.address}
																onChange={handleChange}
																isValid={touched.address && !errors.address}
															/>
															{errors.address && touched.address &&
																<div className="error-msg">
																	{errors.address}
																</div>
															}
														</div>
													</Form.Group>
													<Form.Group as={Col} md="4" controlId="validationFormik03">
														<div className="mb-2">
															<Form.Label>Area</Form.Label>
															<Form.Control
																type="text"
																name="area"
																placeholder="Area"
																value={values.area}
																onChange={handleChange}
																isValid={touched.area && !errors.area}
															/>
															{errors.area && touched.area &&
																<div className="error-msg">
																	{errors.area}
																</div>
															}
														</div>
														<div className="mb-2">
															<Form.Label>Price</Form.Label>
															<Form.Control
																type="number"
																name="price"
																placeholder="Price"
																value={values.price}
																onChange={handleChange}
																isValid={touched.price && !errors.price}
															/>
															{errors.price && touched.price &&
																<div className="error-msg">
																	{errors.price}
																</div>
															}
														</div>
														<div className="mb-2 form-select-box">
															<Form.Label>Bedrooms</Form.Label>
															<Form.Control
																as="select"
																name="bedroom"
																placeholder="Bedrooms"
																value={values.bedroom}
																onChange={handleChange}
																isValid={touched.bedroom && !errors.bedroom}
															>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
															</Form.Control>
															{errors.bedroom && touched.bedroom &&
																<div className="error-msg">
																	{errors.bedroom}
																</div>
															}
														</div>
														<div className="mb-2 form-select-box">
															<Form.Label>Bathrooms</Form.Label>
															<Form.Control
																as="select"
																name="bathroom"
																placeholder="Bathrooms"
																value={values.bathroom}
																onChange={handleChange}
																isValid={touched.bathroom && !errors.bathroom}
															>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
															</Form.Control>
															{errors.bathroom && touched.bathroom &&
																<div className="error-msg">
																	{errors.bathroom}
																</div>
															}
														</div>
														<div className="mb-2 form-select-box">
															<Form.Label>Carpet Area</Form.Label>
															<Form.Control
																as="select"
																name="carpetArea"
																placeholder="Carpet Area"
																value={values.carpetArea}
																onChange={handleChange}
																isValid={touched.carpetArea && !errors.carpetArea}>
																<option value="sq_ft">Sq Ft</option>
																<option value="sq_yd">Sq Yd</option>
																<option value="sq_mt">Sq Mt</option>
															</Form.Control>
															{errors.carpetArea && touched.carpetArea &&
																<div className="error-msg">
																	{errors.carpetArea}
																</div>
															}
														</div>
													</Form.Group>
												</Form.Row>
												<Button type="submit">Save Property</Button>
											</Form>
										)}
									</Formik>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default AddProperty;
