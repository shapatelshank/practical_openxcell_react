import { connect } from "react-redux";
import Property from ".";
import propertyActions from "../../actions";

const mapStateToProps = (state) => {
    return {};
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProperty: (data) => dispatch(propertyActions.addPropertyAction(data))
    };
};

const AddPropertyContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Property);

export default AddPropertyContainer;