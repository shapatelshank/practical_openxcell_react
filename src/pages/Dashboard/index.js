import React, { useEffect, useState } from 'react';
import Head from '../../components/Head';
import { useHistory } from 'react-router-dom';
import getAPIUrl from '../../utils/getAPIUrl';
import PropertyDetails from './propertyDetails';

const Dashboard = (props) => {
	const { getPropertyList, getRecentlyViewPropertyList, propertyList, recentViewedPropertyList, updateProperty } = props;
	const history = useHistory();
	const [showPropertyDetail, setShowPropertyDetail] = useState(false);
	const [propertyDetail, setPropertyDetail] = useState({});
	const [propertyData, setPropertyData] = useState([]);
	const [recentViewedPropertyData, setRecentViewedPropertyData] = useState([]);
	const [page, setPage] = useState(1);

	useEffect(async () => {
		const getPropertyData = await getPropertyList();
		const getRecentViewedPropertyData = await getRecentlyViewPropertyList();
	}, []);

	useEffect(async () => {
		if (propertyList && propertyList.length > 0) {
			setPropertyData(propertyList);
		}
	}, [propertyList]);

	useEffect(async () => {
		if (recentViewedPropertyList && recentViewedPropertyList.length > 0) {
			setRecentViewedPropertyData(recentViewedPropertyList);
		}
	}, [recentViewedPropertyList]);

	const handleAddPropertyClick = () => {
		history.push('/addproperty');
	}

	const handleViewDetailsPage = async (event, data) => {
		event.preventDefault();
		let updateData = {
			viewCounts : Number(data.viewCounts) + 1
		}
		data.viewCounts = updateData.viewCounts;
		const updatePropertyData = await updateProperty(data._id, updateData);
		setPropertyDetail(data);
		setShowPropertyDetail(true);
	}

	const removeViewDetailPage = async () => {
		setShowPropertyDetail(false);
		const getPropertyData = await getPropertyList();
		const getRecentViewedPropertyData = await getRecentlyViewPropertyList();
	}

	const handleFavourite = async (event, data) => {
		event.preventDefault();
		let updateData = {
			isFav : !data.isFav
		}
		const updatePropertyData = await updateProperty(data._id, updateData);
	}

	return (
		<>
			<Head title="Dashboard" />
			<div className="p-3" style={{ height: "100vh", backgroundColor: "#F5F5F5" }}>
				<div className="card mb-3" style={{ width: "100%" }}>
					<div className="card-body d-flex">
						<div className="flex-grow-1 align-self-center">
							<h5 className="card-title">Welcome to Real Estate Dashboard!</h5>
							<p className="card-text">Here you found list of Properties.</p>
						</div>
						<button className="btn btn-primary align-self-center" onClick={() => handleAddPropertyClick()}>Add Property</button>
					</div>
				</div>
				{showPropertyDetail &&
					<div className="row">
						<div className="col">
							<div className="card mb-3">
								<div className="card-body d-flex">
									<h5 className="card-title text-center flex-grow-1 align-self-center">Project Details</h5>
									<button className="btn btn-danger align-self-center" onClick={() => removeViewDetailPage(false)}>Close</button>
								</div>
							</div>
						</div>
						<PropertyDetails propertyData={propertyDetail}></PropertyDetails>
					</div>}
				{!showPropertyDetail && <div className="row">
					<div className="col">
						<div className="card mb-3">
							<div className="card-body">
								<h5 className="card-title text-center">Filters</h5>
							</div>
						</div>
					</div>
					<div className="col-6">
						<div className="card mb-3">
							<div className="card-body">
								<h5 className="card-title text-center">Property List</h5>
							</div>
						</div>
						{propertyData && propertyData.length > 0 ? propertyData.map((data, index) => {
							return (
								<div className="card mb-3" key={index}>
									<div className="d-flex">
										<div className="flex-fill w-50 p-3">
											{data.images && data.images[0] && data.images[0].thumbUrl ? (
												<img className="card-img-top" src={`${getAPIUrl()}${data.images[0].url}`} alt="Card image cap" style={{ height: "300px", width: "100%" }} />
											) : (
												<div className="d-flex align-items-center" style={{ height: "300px", width: "100%", backgroundColor: "gray" }}>
													<i className="fa fa-icon"></i>
												</div>
											)}
										</div>
										<div className="flex-fill w-50 pt-3 pr-3">
											<ul className="list-group">
												<li className="list-group-item">
													<div className="row">
														<div className="col-md-5">
															Property Name
														</div>
														<div className="col-md-7">
															{data.propertyName}
														</div>
													</div>
												</li>
												<li className="list-group-item">
													<div className="row">
														<div className="col-md-5">
															Description
														</div>
														<div className="col-md-7">
															{data.description}
														</div>
													</div>
												</li>
												<li className="list-group-item">
													<div className="row">
														<div className="col-md-5">
															Address
														</div>
														<div className="col-md-7">
															{data.address}
														</div>
													</div>
												</li>
												<li className="list-group-item">
													<div className="row">
														<div className="col-md-5">
															Locality / Area
														</div>
														<div className="col-md-7">
															{data.area}
														</div>
													</div>
												</li>
												<li className="list-group-item">
													<div className="row">
														<div className="col-md-5">
															Price
														</div>
														<div className="col-md-7">
															{data.price}
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div className="card-footer d-flex">
										<div className="align-self-center flex-xl-fill">Views: <span>{data.viewCounts}</span></div>
										<div className="align-self-center">
											<button className="btn btn-info ml-3 mr-3" onClick={(event) => handleViewDetailsPage(event, data)}>View All Details</button>
										</div>
										<div className="align-self-center">
											<button type="button" className={data.isFav ? "btn btn-danger" : "btn btn-light"} onClick={(event) => handleFavourite(event, data)}><i className="fa fa-heart pr-2" aria-hidden="true" onClick={(event) => handleFavourite(event, data)}></i>Add to Favourite</button>
										</div>
									</div>
								</div>
							)
						}) : (
							<div className="card mb-3">
								<div className="card-body">
									<h5 className="card-title text-center">No Property found</h5>
								</div>
							</div>
						)}
					</div>
					<div className="col">
						<div className="card mb-3">
							<div className="card-body">
								<h5 className="card-title text-center">Recently Viewed Property</h5>
							</div>
						</div>
						{recentViewedPropertyData && recentViewedPropertyData.length > 0 ? recentViewedPropertyData.map((data, index) => {
							return (
								<div className="card mb-3" key={index}>
									{data.images && data.images[0] && data.images[0].thumbUrl ? (
										<img className="card-img-top" src={`${getAPIUrl()}${data.images[0].thumbUrl}`} alt="Card image cap" style={{ height: "300px", width: "100%" }} />
									) : (
										<div className="d-flex align-items-center" style={{ height: "300px", width: "100%", backgroundColor: "gray" }}>
											<i className="fa fa-icon"></i>
										</div>
									)}

									<div className="card-body">
										<h5 className="card-title">{data.propertyName}</h5>
										<p className="card-text">{data.description}</p>
									</div>
									<div className="card-footer d-flex">
										<div className="align-self-center flex-grow-1">Views: <span>{data.viewCounts}</span></div>
										<div className="align-self-center">
											<button className="btn btn-info" onClick={(event) => handleViewDetailsPage(event, data)}>View All Details</button>
										</div>
									</div>
								</div>
							)
						}) : (
							<div className="card mb-3">
								<div className="card-body">
									<h5 className="card-title text-center">No Recently Viewed Property found</h5>
								</div>
							</div>
						)}
					</div>
				</div>}
			</div>
		</>
	);
};

export default Dashboard;
