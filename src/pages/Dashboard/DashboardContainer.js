import { connect } from "react-redux";
import Dashboard from ".";
import propertyActions from "../../actions";

const mapStateToProps = (state) => {
    return {
        propertyList: state.propertyList,
        recentViewedPropertyList: state.recentViewedPropertyList,
        propertyDetail: state.propertyDetail
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getPropertyList: (page) => dispatch(propertyActions.getPropertiesAction(page)),
        getRecentlyViewPropertyList: () => dispatch(propertyActions.getRecentlyViewPropertyAction()),
        getPropertyDetails: () => dispatch(propertyActions.getPropertyDetailsAction()),
        updateProperty: (id, data) => dispatch(propertyActions.updatePropertyAction(id, data)),
    };
};

const DashboardContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);

export default DashboardContainer;