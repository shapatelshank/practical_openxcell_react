import React from 'react';
import getAPIUrl from '../../utils/getAPIUrl';

const PropertyDetails = (props) => {
    const { propertyData } = props;

    return (
        <>
            <div className="col-12">
                {propertyData ? (
                    <div className="card mb-3">
                        <div className="w-100" style={{ textAlign: "center" }}>
                            <div className="p-3 w-75" style={{ display: "inline-block" }}>
                                {propertyData.images && propertyData.images.length > 0 ? (
                                    <div >
                                        <div className="d-flex">
                                            { propertyData.images.map((image, index) => {
                                            return ( <div class="flex-fill p-3" style={{ border: "1px solid rgba(0,0,0,.125)", margin: "10px"}}>
                                                <img src={`${getAPIUrl()}${image.url}`} alt={`Image ${index}`}  style={{ height: "300px", width: "300px" }}/>
                                            </div>
                                            )})}
                                        </div>
                                    </div>
                                ) : (
                                    <div className="d-flex align-items-center" style={{ height: "300px", width: "100%", backgroundColor: "gray" }}>
                                        <i className="fa fa-icon"></i>
                                    </div>
                                )}
                            </div>
                            <div className="p-3 w-75" style={{ display: "inline-block" }}>
                                <ul className="list-group">
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Property Name
                                            </div>
                                            <div className="col-md-7">
                                                {propertyData.propertyName}
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Description
                                            </div>
                                            <div className="col-md-7">
                                                {propertyData.description}
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Address
                                            </div>
                                            <div className="col-md-7">
                                                {propertyData.address}
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Locality / Area
                                            </div>
                                            <div className="col-md-7">
                                                {propertyData.area}
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Price
                                            </div>
                                            <div className="col-md-7">
                                                {propertyData.price}
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Bedrooms
                                            </div>
                                            <div className="col-md-7">
                                                {propertyData.bedroom}
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Bathrooms
                                            </div>
                                            <div className="col-md-7">
                                                {propertyData.bathroom}
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Total View Counts
                                            </div>
                                            <div className="col-md-7">
                                                {propertyData.viewCounts}
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-5">
                                                Is Your Favourite
                                            </div>
                                            <div className="col-md-7">
                                            <button type="button" className={propertyData.isFav ? "btn btn-danger" : "btn btn-light"}><i className="fa fa-heart" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="card mb-3">
                        <div className="card-body">
                            <h5 className="card-title text-center">No Property Details found</h5>
                        </div>
                    </div>
                )}
            </div>
        </>
    );
};

export default PropertyDetails;
