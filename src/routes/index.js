import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const DashboardContainer = lazy(() => import('../pages/Dashboard/DashboardContainer'));
const AddPropertyContainer = lazy(() => import('../pages/AddProperty/AddPropertyContainer'));

const Routes = () => {
	return (
		<Suspense fallback={''}>
			<Router>
				<Switch>
					<Route exact path="/" component={DashboardContainer} />
					<Route exact path="/addproperty" component={AddPropertyContainer} />
				</Switch>
			</Router>
		</Suspense>
	);
};

export default Routes;
