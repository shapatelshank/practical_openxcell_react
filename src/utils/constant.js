export const PAGE = 1;
export const LIMIT = 5;
export const ORDERBY = 'desc';
export const VALID_IMAGE_FILE_TYPES = ['image/jpeg', 'image/jpg', 'image/png'];