import { propertyConstants } from '../actions/contants';

const initState = {
    propertyList: [],
    recentViewedPropertyList: []
}
function users(state = { initState }, action) {
    switch (action.type) {
        case propertyConstants.GET_PROPERTIES_SUCCESS:
            return {
                ...state,
                propertyList: action.properties
            };
        case propertyConstants.GET_RECENT_VIEWED_PROPERTIES_SUCCESS:
            return {
                ...state,
                recentViewedPropertyList: action.properties
            };
        default:
            return state
    }
}

export default users;