import { propertyConstants } from './contants';
import addProperty from '../services/property/addProperty';
import getProperties from '../services/property/getProperties';
import getRecentViewedProperties from '../services/property/getRecentViewedProperties';
import updateProperty from "../services/property/updateProperty";

const propertyActions = {
    addPropertyAction,
    getPropertiesAction,
    getRecentlyViewPropertyAction,
    updatePropertyAction
};

export default propertyActions;

function addPropertyAction(propertyData) {
    return dispatch => {
        dispatch(request());
        addProperty(propertyData)
            .then(
                property => {
                    dispatch(success());
                }
            );
    };

    function request() { return { type: propertyConstants.ADD_PROPERTY_REQUEST } }
    function success() { return { type: propertyConstants.ADD_PROPERTY_SUCCESS } }
}

function getPropertiesAction() {
    return dispatch => {
        dispatch(request());

        getProperties()
            .then(
                property => {
                    if (property) {
                        dispatch(success(property.data));
                    }
                }
            );
    };

    function request() { return { type: propertyConstants.GET_PROPERTIES_REQUEST } }
    function success(properties) { return { type: propertyConstants.GET_PROPERTIES_SUCCESS, properties } }
}

function getRecentlyViewPropertyAction() {
    return dispatch => {
        dispatch(request());

        getRecentViewedProperties()
            .then(
                property => {
                    if (property) {
                        dispatch(success(property.data));
                    }
                }
            );
    };

    function request() { return { type: propertyConstants.GET_RECENT_VIEWED_PROPERTIES_REQUEST } }
    function success(properties) { return { type: propertyConstants.GET_RECENT_VIEWED_PROPERTIES_SUCCESS, properties } }
}

function updatePropertyAction(id, data) {
    return dispatch => {
        dispatch(request());

        updateProperty(id, data)
            .then(
                property => {
                    dispatch(success());
                    getProperties()
                        .then(
                            property => {
                                if (property) {
                                    dispatch(getSuccess(property.data));
                                }
                            }
                        );
                    getRecentViewedProperties()
                        .then(
                            property => {
                                if (property) {
                                    dispatch(getRecentSuccess(property.data));
                                }
                            }
                        );
                }
            );
    };

    function request() { return { type: propertyConstants.UPDATE_PROPERTY_REQUEST } }
    function success() { return { type: propertyConstants.UPDATE_PROPERTY_SUCCESS } }
    function getSuccess(properties) { return { type: propertyConstants.GET_PROPERTIES_SUCCESS, properties } }
    function getRecentSuccess(properties) { return { type: propertyConstants.GET_RECENT_VIEWED_PROPERTIES_SUCCESS, properties } }
}