import getAPIUrl from '../../utils/getAPIUrl';
import errorHandler from '../../utils/errorHandler';
const axios = require('axios');

export default async function getProperties() {
	try {
		const query = "limit=10&orderBy=createdAt";
		const response = await axios({
			method: 'get',
			url: `${getAPIUrl()}/api/properties?${query}`,
			headers: {
				'Content-Type': 'application/json'
			},
		});
		if (response.status === 200) {
			return response.data;
		} else {
			return false;
		}
	} catch (error) {
		await errorHandler(error);
		return false;
	}
}
