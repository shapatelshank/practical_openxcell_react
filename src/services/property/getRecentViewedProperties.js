import getAPIUrl from '../../utils/getAPIUrl';
import errorHandler from '../../utils/errorHandler';
const axios = require('axios');

export default async function getRecentViewedProperties() {
	try {
		const query = "limit=5&orderBy=recentlyViewed";
		const response = await axios({
			method: 'get',
			url: `${getAPIUrl()}/api/properties?${query}`,
			headers: {
				'Content-Type': 'application/json'
			},
		});
		if (response.status === 200) {
			return response.data;
		} else {
			return false;
		}
	} catch (error) {
		await errorHandler(error);
		return false;
	}
}
