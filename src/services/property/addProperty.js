import getAPIUrl from '../../utils/getAPIUrl';
import errorHandler from '../../utils/errorHandler';
const axios = require('axios');

export default async function addProperty(data) {
    try {
        const headers = {
            'Content-Type': 'multipart/form-data'
        }
        const response = await axios.post(`${getAPIUrl()}/api/addProperty`, data, headers);
        if (response.status === 200) {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        await errorHandler(error);
        return false;
    }
}
